<?php

use App\Http\Controllers\NoteController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->prefix('/notes')->group(function () {
    Route::get('', [NoteController::class, 'show']);
    Route::get('/archived', [NoteController::class, 'showArchived']);

    Route::delete('/{id}', [NoteController::class, 'delete']);
//
    Route::put('/{id}', [NoteController::class, 'update']);

    Route::post('', [NoteController::class, 'store']);
    Route::patch('/{id}/archived', [NoteController::class, 'archive']);
    Route::patch('/{id}/pin', [NoteController::class, 'pin']);
});

Route::post('/login', [UserController::class, 'login']);
Route::post('/signin', [UserController::class, 'signin']);
