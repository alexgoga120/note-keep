<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginDataRequest;
use App\Http\Requests\SigninDataRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function login(LoginDataRequest $request): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $data = json_decode($request->getContent());
            $user = $this->userRepository->getByEmail($data->email);
            if (!$user) {
                $response["error"] = ["Usuario no encontrado."];
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
            if (!Hash::check($data->password, $user->password)) {
                $response["error"] = ["Credenciales incorrectas."];
                return response()->json($response, Response::HTTP_BAD_REQUEST);
            }
            $token = $user->createToken("login_token");
            $response["data"]["token"] = $token->plainTextToken;
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = ["Error en servidor, contecte a administradpr"];
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function signin(SigninDataRequest $request): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $user = new User();
            $user->fill($request->all());
            $this->userRepository->save($user);
            $token = $user->createToken("login_token");
            $response["status"] = 200;
            $response["data"]["token"] = $token->plainTextToken;

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr";
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
