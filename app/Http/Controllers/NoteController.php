<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteCreateDataRequest;
use App\Models\Note;
use App\Models\User;
use App\Repositories\NoteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends Controller
{
    private $noteRepositories;

    public function __construct(NoteRepository $noteRepositories)
    {
        $this->noteRepositories = $noteRepositories;
    }

    public function show(): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->getActive();
            $response['data'] = $note;
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showArchived(): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->getArchived();
            $response['data'] = $note;
            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
            return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(NoteCreateDataRequest $request): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = new Note();
            $note->fill($request->all());
            $note->fk_id_user = Auth::id();
            $this->noteRepositories->save($note);
            $response['data'] = "¡Nota Creada!";

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
        }
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function update(NoteCreateDataRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->get($id);
            $note->title = $request->title;
            $note->body = $request->body;
            $this->noteRepositories->save($note);
            $response['data'] = "¡Nota Actualizada!";

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
        }
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function pin($id): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->get($id);
            $note->is_pinned = !$note->is_pinned;
            $this->noteRepositories->save($note);
            $response['data'] = "¡Nota Archivada!";

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
        }
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function archive($id): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->get($id);
            $note->is_archived = !$note->is_archived;
            $this->noteRepositories->save($note);
            $response['data'] = "¡Nota Archivada!";

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
        }
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {
        $response = [];
        try {
            $note = $this->noteRepositories->get($id);
            $this->noteRepositories->delete($note);
            $response['data'] = "¡Nota Borrada!";

            return response()->json($response, Response::HTTP_OK);
        } catch (\Exception $ex) {
            $response["error"] = "Error en servidor, contecte a administradpr && " . $ex;
        } catch (\Throwable $e) {
            $response["error"] = "Error al guardar nota && " . $e;
        }
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
