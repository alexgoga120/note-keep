<?php

namespace App\Repositories;

use App\Models\Note;
use Illuminate\Support\Facades\Auth;

class NoteRepository extends BaseRepository
{
    public function __construct(Note $note)
    {
        parent::__construct($note);
    }

    public function getActive()
    {
        return $this->model->whereFkIdUser(Auth::id())
            ->where('is_archived', false)
            ->orderByDesc('is_pinned')->get();
    }

    public function getArchived()
    {
        return $this->model->whereFkIdUser(Auth::id())->where('is_archived', true)->get();
    }

}
