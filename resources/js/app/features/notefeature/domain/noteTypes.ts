const TYPES = {
    NotePort: Symbol("NotePort"),
    NoteDataSource: Symbol("NoteDataSource"),
    CreateNoteUseCase: Symbol("CreateNoteUseCase"),
    ModifyNoteUseCase: Symbol("ModifyNoteUseCase"),
    ListNoteUseCase: Symbol("ListNoteUseCase"),
    ListArchivedNoteUseCase: Symbol("ListArchivedNoteUseCase"),
    DeleteNoteUseCase: Symbol("DeleteNoteUseCase"),
    ArchiveNoteUseCase: Symbol("ArchiveNoteUseCase"),
    PinNoteUseCase: Symbol("PinNoteUseCase"),
};

export default TYPES;
