export default interface SigninData {
    name: string;
    email: string;
    password: string;
}
