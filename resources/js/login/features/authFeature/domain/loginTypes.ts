const TYPES = {
    LoginPort: Symbol("LoginPort"),
    LoginDataSource: Symbol("LoginDataSource"),
    LoginUseCase: Symbol("LoginUseCase"),
    SigninUseCase: Symbol("SigninUseCase")
};

export default TYPES;
