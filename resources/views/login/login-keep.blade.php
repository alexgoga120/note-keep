<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>Login Note keep</title>
</head>
<body>
<div id="login_app"></div>
@vite('resources/js/login/login-main.ts')
</body>
</html>
