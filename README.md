
# Note keep - Alex Gomez Garcia


## Instalación

Npm version: 8.3.1
php version: 8.2.0
composer version:  2.2.7

En terminal.

```bash
    git clone https://gitlab.com/alexgoga120/note-keep.git
    cd note-keep
```


Instalar dependencias.

```bash
    npm install
    composer update 
```

Para la base de datos se ocupo MySQL con Xampp, usando el puerto 3305 y localhost (127.0.0.1) (Si se necesita otro puerto cambiarlo en .env).


Correr migraciones con MySQL funcionando.

```bash
    php artisan migrate
```

En caso de fallo en creacion de la base de datos, crearla con nombre note-keep.

Para correr el proyecto.


```bash
    npm run dev
    php artisan serve
```

Si se tienen las configuracion por defecto el servidor estara accesible en http://127.0.0.1:8000/

En caso contrario en consola mostrara la dirección.
